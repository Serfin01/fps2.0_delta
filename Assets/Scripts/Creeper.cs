﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Creeper : MonoBehaviour
{
    public enum State { Stationary, Patrol, Chase, Attack, Stunned, Dead };
    public State state;

    private NavMeshAgent agent;
    private Animator anim;
    private AudioPlayer sound;
    public AudioSource Foot;
    [Header("Properties")]
    public int maxLife;
    public int currentLife;
    [Header("Stationary")]
    public float stopTime = 1.0f;
    private float timeCounter = 0.0f;
    [Header("Patrol")]
    public Transform[] patrolPoints;
    private int currentPoint;
    public bool stopOnEachPoint;
    private bool nearPoint;
    public float patrolSpeed;
    [Header("Chase")]
    public LayerMask targetMask;
    private bool targetDetected;
    private Transform targetTransform;
    public float rangeDetection = 8 ;
    public float findRange ;
    public float lostRange ;
    public float chaseSpeed ;
    [Header("Stunned")]
    public float stunTime = 0.25f;
    public Color stunColor = Color.red;
    //public Material material;
    private Renderer[] rend;
    private MaterialPropertyBlock block;
    [Header("Attack")]
    public float explosionRange = 6;
    public int explosionDamage = 25;
    public float explosionForce = 10;
    public ParticleSystem explosionPS;
    [Header("Dead")]
    private Collider physicCollider;
    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        sound = GetComponentInChildren<AudioPlayer>();
        physicCollider = GetComponent<Collider>();

        currentLife = maxLife;

        nearPoint = true;

        //material = GetComponentInChildren<MeshRenderer>().material;
        //material = GetComponentInChildren<MeshRenderer>().sharedMaterial;
        //material.SetColor("_Color", stunColor);

        block = new MaterialPropertyBlock();
        rend = GetComponentsInChildren<Renderer>();
        rend[0].GetPropertyBlock(block);

        SetStationary();
    }
    private void Update()
    {
        switch (state)
        {
            case State.Stationary:
                UpdateStationary();
                break;
            case State.Patrol:
                UpdatePatrol();
                break;
            case State.Chase:
                UpdateChase();
                break;
            case State.Attack:
                UpdateAttack();
                break;
            case State.Stunned:
                UpdateStunned();
                break;
            case State.Dead:
                UpdateDead();
                break;
            default:
                break;
        }
    }

    #region Sets
    void SetStationary()
    {
        agent.isStopped = true;
        anim.SetBool("isMoving", false);
        timeCounter = 0;

        rangeDetection = findRange;

        //sound.Play(Random.Range(3, 3));

        state = State.Stationary;
    }
    void SetPatrol()
    {
        agent.isStopped = false;
        agent.stoppingDistance = 0;
        agent.speed = patrolSpeed;

        if (nearPoint) GoToNearPoint();
        else GoToNextPoint();

        anim.SetBool("isMoving", true);

        state = State.Patrol;
    }
    void SetPatrolFromStunned()
    {
        agent.isStopped = false;
        agent.stoppingDistance = 0;
        agent.speed = patrolSpeed;

        anim.SetBool("isMoving", true);

        state = State.Patrol;
    }
    void SetChase()
    {
        rangeDetection = lostRange;

        agent.isStopped = false;
        agent.stoppingDistance = 2.0f;
        agent.speed = chaseSpeed;

        anim.SetBool("isMoving", true);

        nearPoint = true;

        state = State.Chase;
    }
    void SetAttack()
    {
        agent.isStopped = true;

        anim.SetBool("isMoving", false);
        anim.SetTrigger("Explode");
        sound.Play(1,8f);

        state = State.Attack;
    }
    void SetStunned()
    {
        block.SetColor("_Color", stunColor);
        for (int i = 0; i < rend.Length; i++)
        {
            rend[i].SetPropertyBlock(block);
        }

        agent.isStopped = true;
        anim.SetBool("isMoving", false);

        timeCounter = 0;

        state = State.Stunned;
    }
    void SetDead()
    {
        agent.isStopped = true;
        physicCollider.enabled = false;

        anim.SetBool("isMoving", false);
        anim.SetTrigger("Die"); //ACTIVAR ANIMACION DE MUERTE
        sound.Play(1);

        Destroy(gameObject, 4.0f);

        state = State.Dead;
    }
    void SetDeadExplosion()
    {
        agent.isStopped = true;
        physicCollider.enabled = false;

        anim.SetBool("isMoving", false);

        Destroy(gameObject, 4.0f);

        state = State.Dead;
    }
    #endregion

    #region Updates
    void UpdateStationary()
    {
        TrackingTarget();
        if (targetDetected)
        {
            SetChase();
            return;
        }

        if (timeCounter >= stopTime)
        {
            SetPatrol();
        }
        else timeCounter += Time.deltaTime;
    }
    void UpdatePatrol()
    {
        TrackingTarget();
        if (targetDetected)
        {
            SetChase();
            return;
        }

        if (agent.remainingDistance < 0.1f)
        {
            if (stopOnEachPoint)
            {
                SetStationary();
            }
            else
            {
                GoToNextPoint();
            }
        }
    }
    void UpdateChase()
    {
        TrackingTarget();
        if (!targetDetected)
        {
            SetStationary();
            return;
        }

        agent.SetDestination(targetTransform.position);

        if (agent.remainingDistance <= agent.stoppingDistance)
        {
            SetAttack();
        }
    }
    void UpdateAttack()
    {

    }
    void UpdateStunned()
    {
        if (timeCounter >= stunTime)
        {
            block.SetColor("_Color", Color.white);
            for (int i = 0; i < rend.Length; i++)
            {
                rend[i].SetPropertyBlock(block);
            }

            SetPatrolFromStunned();
        }
        else timeCounter += Time.deltaTime;
    }
    void UpdateDead()
    {

    }
    #endregion

    void GoToNextPoint()
    {
        currentPoint++;
        if (currentPoint >= patrolPoints.Length) currentPoint = 0;

        agent.SetDestination(patrolPoints[currentPoint].position);
    }
    void GoToNearPoint()
    {
        nearPoint = false;

        float distance = Mathf.Infinity;

        for (int i = 0; i < patrolPoints.Length; i++)
        {
            float currentDistance = Vector3.Distance(transform.position, patrolPoints[i].position);
            if (currentDistance < distance)
            {
                distance = currentDistance;
                currentPoint = i;
            }
        }

        agent.SetDestination(patrolPoints[currentPoint].position);
    }

    void TrackingTarget()
    {
        targetDetected = false;

        Collider[] hits = Physics.OverlapSphere(transform.position, rangeDetection, targetMask);
        if (hits.Length > 0)
        {
            targetDetected = true;
            targetTransform = hits[0].transform;
        }
    }

    public void Damage(int value)
    {
        currentLife -= value;

        if (currentLife <= 0)
        {
            SetDead();
        }
        else
        {
            sound.Play(2,1.2f);
            SetStunned();
        }
    }
    public void EndExplosion()
    {
        sound.Play(Random.Range(0, 0));
        explosionPS.Play();

        Collider[] hits = Physics.OverlapSphere(transform.position, explosionRange);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].tag == "Player")
            {
                hits[i].GetComponent<PlayerController>().Damage(explosionDamage);
                //hits[i].SendMessage("Damage", explosionDamage, SendMessageOptions.DontRequireReceiver);
            }
            // if(hits[i].gameObject.layer == LayerMask.NameToLayer("Player"))
        }

        SetDeadExplosion();
    }
    private void OnDrawGizmos()
    {
        if (targetDetected) Gizmos.color = Color.red;
        else Gizmos.color = Color.blue;

        Gizmos.DrawWireSphere(transform.position, rangeDetection);
    }
}