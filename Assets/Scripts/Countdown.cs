﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Countdown : MonoBehaviour
{
    public Animator animLogo;

    private int levelToLoad;

    private float timeCounter;

    public float basicWait;

    public float speed;


    private void Start()
    {
        
        StartCoroutine(Wait(basicWait));
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            FadeToLevel(1);
        }
    }

    public void FadeToLevel (int levelindex)
    {
        levelToLoad = levelindex;
        animLogo.SetTrigger("FadeOut");
    }

    public void OnFadeComplete ()
    {
        SceneManager.LoadScene(levelToLoad);
    }



    public void Change()
    {
        FadeToLevel(1);
    }

    IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(time);
        Change();
    }

}
