﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ButtonsFunctions : MonoBehaviour
{
    public Transform ContainerButtons;

    public AudioSource Click;
 

    

    public void Canvas()
    {
        {
            Click.Play();
            if (ContainerButtons.gameObject.activeInHierarchy == false)
            {
                ContainerButtons.gameObject.SetActive(true);

            }
            else
            {
                ContainerButtons.gameObject.SetActive(false);

            }
        }
    }
}
