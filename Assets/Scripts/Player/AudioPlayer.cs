﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    public AudioClip[] clips;

    public void Play(int index)
    {
        Play(index, 1.0f);
    }

    public void Play(int index, float volume)
    {
        Play(index, volume, 1.0f);
    }

    public void Play(int index, float volume, float pitch)
    {
        // Crear audiosource
        AudioSource source = gameObject.AddComponent<AudioSource>();

        // Configurar audiosource
        source.playOnAwake = false;
        source.clip = clips[index];
        source.volume = volume;
        source.pitch = pitch;
        source.spatialBlend = 1;
        

        // Reproducir audio
        source.Play();

        // Eliminar el source cuando el audio termine de reproducirse
        Destroy(source, clips[index].length);
    }
}
