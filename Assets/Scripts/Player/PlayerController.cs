﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// en este script no hacemos INPUTS!! NITAMOS OTRO PA PONERLOS...

public class PlayerController : MonoBehaviour
{
    private CharacterController controller; 



    public float gravityMagnitude = 1.0f; 

    private Vector2 movementAxis; 
    private bool jump = false; 
    private Vector3 moveDirection;

    [Header("Properties")]

    public float forceToGround = Physics.gravity.y; // !!!
    public float jumpSpeed = 5;
    public float moveSpeed = 5;
    public float maxLife = 100;
    public float currentLife;

    private PlayerUI ui;

    private void Start()
    {
        controller = GetComponent<CharacterController>();
        currentLife = maxLife;

        ui = FindObjectOfType<PlayerUI>();
        ui.UpdateLifeUI(currentLife, maxLife);
    }

    private void Update() 
    {
        GravitySimulation();
        MovementSimulation();
        

        controller.Move(moveDirection * Time.deltaTime);
    }

    
    private void GravitySimulation()
    {
        
        if (controller.isGrounded && !jump) 
        {
           
            moveDirection.y = forceToGround; 
        }
        else 
        {
            moveDirection += Physics.gravity * gravityMagnitude * Time.deltaTime; 

            jump = false;
        }
        
    }

    private void MovementSimulation()
    {
        Vector3 localDirection = transform.forward * movementAxis.y + transform.right * movementAxis.x;

        moveDirection.x = localDirection.x * moveSpeed;
        moveDirection.z = localDirection.z * moveSpeed; 
    }

    public void StartJump() 
    {
        if (controller.isGrounded) 
        {
            moveDirection.y = jumpSpeed;
            jump = true; 
        }
    }

    
    public void SetAxis(Vector2 inputAxis) 
    {
        movementAxis = inputAxis;
    }

    public void Damage(int hit)
    {
        currentLife -= hit;
            
        if(currentLife <= 0)
        {
            currentLife = 0;
            Dead();
        }
        //UpdateLifeUI
        ui.UpdateLifeUI(currentLife, maxLife);
    }

    public void Dead()
    {
        SceneManager.LoadScene(4);
    }
}
