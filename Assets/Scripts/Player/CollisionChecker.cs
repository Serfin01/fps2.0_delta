﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionChecker : MonoBehaviour
{
    [Header("Collision state")]
    public bool isGrounded;
    public bool justGrounded;
    public bool justNotGrounded;
    public bool wasGrounded;
    public bool isTouchingWall;
    [Header("Ground")]
    public Vector3 gOffset;
    public float gDist;
    public float gDistBtRays;
    public LayerMask gMask;
    [Header("Wall")]
    public Vector3 wOffset;
    public float wDist;
    public float wDistBtRays;
    public Vector3 wDir = Vector3.right;

    public void CheckCollisions()
    {
        CheckGround();
        CheckWall();
    }

    void CheckGround()
    {
        wasGrounded = isGrounded;
        justGrounded = false;
        justNotGrounded = false;
        isGrounded = false;

        Vector3 origin = transform.position + gOffset;
        int sign = 1;

        RaycastHit hit = new RaycastHit();
        for (int i = 0; i < 3; i++)
        {
            if (Physics.Raycast(origin, Vector3.down, out hit, gDist, gMask))
            {
                if (hit.normal.y >= 0.7f)
                {
                    isGrounded = true;
                    break;
                }
            }

            origin.x += sign * (i + 1) * gDistBtRays;
            sign *= -1;
        }

        if (!wasGrounded && isGrounded) justGrounded = true;
        else if (wasGrounded && !isGrounded) justNotGrounded = true;

    }
    void CheckWall()
    {
        isTouchingWall = false;

        Vector3 origin = transform.position + wOffset;
        int sign = 1;

        RaycastHit hit = new RaycastHit();
        for (int i = 0; i < 3; i++)
        {
            if (Physics.Raycast(origin, wDir, out hit, wDist, gMask))
            {
                if (Mathf.Abs(hit.normal.x) >= 0.85f)
                {
                    isTouchingWall = true;
                    break;
                }
            }

            origin.y += sign * (i + 1) * wDistBtRays;
            sign *= -1;
        }
    }

    public void Flip()
    {
        wDir.x *= -1;
    }

    void DrawGroundRays()
    {
        if (isGrounded) Gizmos.color = Color.cyan;
        else Gizmos.color = Color.red;

        Vector3 origin = transform.position + gOffset;
        int sign = 1;

        for (int i = 0; i < 3; i++)
        {
            Gizmos.DrawRay(origin, Vector3.down * gDist);
            origin.x += sign * (i + 1) * gDistBtRays;

            sign *= -1;
        }
    }
    void DrawWallRays()
    {
        if (isTouchingWall) Gizmos.color = Color.blue;
        else Gizmos.color = Color.magenta;

        Vector3 origin = transform.position + wOffset;
        int sign = 1;

        for (int i = 0; i < 3; i++)
        {
            Gizmos.DrawRay(origin, wDir * wDist);

            origin.y += sign * (i + 1) * wDistBtRays;
            sign *= -1;
        }
    }

    private void OnDrawGizmos()
    {
        DrawGroundRays();
        DrawWallRays();
    }
}