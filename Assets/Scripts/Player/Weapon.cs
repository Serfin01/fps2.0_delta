﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Weapon : MonoBehaviour
{

    public Animator animator;
    private AudioSource Sound;
    private int LoadIdle;

    [Header("Properties")]
    public int maxAmmo;
    public int currentAmmo;
    public float fireRate;
    public float reloadTime;
    public float hitForce;
    public int hitDamage;

    [Header("State")]
    public bool reloading;
    public bool isShoting;
    //public ParticleSystem ShootingPS;

    [Header("Raycast")]
    public LayerMask targetMask;
    public float range = Mathf.Infinity;
    private Camera maincamera;

    private PlayerUI ui;
    

    private void Start()
    {
        maincamera = Camera.main;
        Sound = GetComponentInChildren<AudioSource>();
        animator = GetComponentInChildren<Animator>();

        currentAmmo = maxAmmo;
        reloading = false;
        isShoting = false;

        ui = FindObjectOfType<PlayerUI>();
        ui.UpdateAmmoUI(currentAmmo, maxAmmo);
    }

   



    public void Shot()
    {
       
      
        
        if (isShoting || reloading) return;
        if (currentAmmo <= 0)
        {

            
            Reload();
            return;
        }



        isShoting = true;

        currentAmmo--;
        ui.UpdateAmmoUI(currentAmmo, maxAmmo);
        

        Ray ray = maincamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        Debug.DrawRay(ray.origin, ray.direction, Color.red, 1);

        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, range, targetMask))
        {
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(ray.direction * hitForce, ForceMode.Impulse);
            }

            // Enemigos???
            if (hit.transform.tag == "Enemy")
            {
                //hit.transform.GetComponent<Creeper>().Damage(hitDamage);
                hit.transform.SendMessage("Damage", hitDamage);
            }
        }

        StartCoroutine(Shoting());
    }

    public void Reload()
    {
      
        if (isShoting || reloading) return;

        reloading = true;


        StartCoroutine(Reloading());
    }

    public void FadeToLevel(int levelindex)
    {
        LoadIdle = levelindex;
        reloading = false;
        isShoting = false;
        
    }


    IEnumerator Shoting()
    {
        Sound.Play(07);
        animator.SetBool("Shooting", true);
      
        yield return new WaitForSeconds(fireRate);
    
        animator.SetBool("Shooting", false);
        isShoting = false;

    }

    
    IEnumerator Reloading()
    {
      
        animator.SetBool("Reloading", true);
        
        yield return new WaitForSeconds(reloadTime);


        animator.SetBool("Reloading", false);
        
        currentAmmo = maxAmmo;
        reloading = false;
        ui.UpdateAmmoUI(currentAmmo, maxAmmo);


        
        Sound.Play(07);
    }
    
}