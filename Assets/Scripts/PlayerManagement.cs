﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManagement : MonoBehaviour
{
    private Vector2 axis;
    private CharacterController controller;      
    public float speed;
    public Vector3 moveDirection;
    public float jumpSpeed;
    private bool jump;
    public float gravityMagnitude = 1.0f;
    public float maxLife = 100;
    public float currentLife;

    private Vector3 transformDirection;
    public GameObject mano;
    public GameObject lanzacohetes;

    private PlayerUI ui;


    [SerializeField] private FireTemplate bullet;
    [SerializeField] private FireTemplate rocketBullet;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        currentLife = maxLife;

        ui.UpdateLifeUI(currentLife, maxLife);
    }

    void Update()
    {          
        transformDirection = axis.x * transform.right + axis.y * transform.forward;

        moveDirection.x = transformDirection.x * speed;
        moveDirection.z = transformDirection.z * speed;

        if (controller.isGrounded && !jump)
        {
            moveDirection.y = Physics.gravity.y * gravityMagnitude * Time.deltaTime;
        }
        else
        {
            jump = false;
            moveDirection.y += Physics.gravity.y * gravityMagnitude * Time.deltaTime;
        }


        controller.Move(moveDirection * Time.deltaTime);
    }


    public void SetAxis(Vector2 inputAxis)
    {
        axis = inputAxis;
    }

    public void Fire()
    {
        Debug.Log("Fire");

        // Instanciar una pelota
        FireTemplate pelota = Instantiate(bullet, mano.transform.position, mano.transform.rotation) as FireTemplate;

        // Ponerla en la posición del player

        // Dispararla
        pelota.Fire();
    }

    public void Fire2()
    {
        Debug.Log("Fire2");

        // Instanciar una pelota
        FireTemplate misil = Instantiate(rocketBullet, lanzacohetes.transform.position, lanzacohetes.transform.rotation) as FireTemplate;

        // Ponerla en la posición del player

        // Dispararla
        misil.Fire();
    }

    public void Damage(int hit)
    {
        currentLife -= hit;

        if (currentLife <= 0)
        {
            currentLife = 0;
            Dead();
        }
        //UpdateLifeUI
        ui.UpdateLifeUI(currentLife, maxLife);
    }

    public void Jump()
    {
        if (!controller.isGrounded) return;

        moveDirection.y = jumpSpeed;
        jump = true;
    }

    public void Dead()
    {
        SceneManager.LoadScene(4);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "lava")
        {
            SceneManager.LoadScene(4);
        }

        if (collision.gameObject.tag == "win")
        {
            SceneManager.LoadScene(3);
        }
    }

    /*
    void OnCollisionEnter(Collision other)
    {
        if (other.tag == "Enemy")
        {
            SceneManager.LoadScene(4);
        }
    }
    */

    /*
        if (controller.isGrounded && !jump)
            {
                moveDirection.y = forceToGround;
            }
            else
            {
                jump = false;
                moveDirection.y += Physics.gravity.y* gravityMagnitude * Time.deltaTime;
            }

            transformDirection = axis.x* transform.right + axis.y* transform.forward;

            moveDirection.x = transformDirection.x* speed;
            moveDirection.z = transformDirection.z* speed;

            controller.Move(moveDirection* Time.deltaTime);
    */
}
